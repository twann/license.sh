# license.sh

This is a small tool that allows you to automatically apply the license you want to your project.

## How to use

Simply use the SPDX short identifier of the license you want to use and run:

```
./license.sh [license] > LICENSE
```

With the GNU General Public License, for example:

```
./license.sh GPL-3.0 > LICENSE
```

## A license is missing

It is probably the case. Just open an issue and it'll be added (unless it isn't open source).

## License

This tool is released into the public domain (see LICENSE).

